#!/bin/bash -eu

set -o pipefail

if [[ ! -e .venv/ ]]; then
  python3 -m venv .venv
fi
. .venv/bin/activate
pip3 install -r features/requirements.txt

behave "${@}"
