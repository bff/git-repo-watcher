use std::path::PathBuf;

use color_eyre::Result;
use git_event::GitRepoWatchHandler;
use git_meta;
use git_url_parse::GitUrl;

#[tokio::main]
async fn main() -> Result<()> {
    let branch = "master".into();
    let test_url = "git@gitlab.com:bff/0x50c.git";
    let test_url = GitUrl::parse(test_url).unwrap();
    let ssh_private_key_path = PathBuf::from("/home/bff/.ssh/gitlab/bff");
    let ssh_user = "git".into();
    let clone_creds = git_meta::GitCredentials::SshKey {
        username: ssh_user,
        public_key: None,
        private_key: ssh_private_key_path,
        passphrase: None,
    };

    let mut watcher = GitRepoWatchHandler::new(test_url.to_string())?
        .with_branch(Some(branch))
        .with_credentials(Some(clone_creds))
        .with_shallow_clone(true);

    let _ = watcher
        .watch_new_commits(true, move |state| {
            println!();
            println!("Last updated: {:?}", state.last_updated);

            for (branch, meta) in state.branch_heads {
                println!("Branch: {}", branch);
                println!("Commit id: {}", meta.id);
                println!("Commit message: {}", meta.message.unwrap());
                println!("Timestamp: {:?}", meta.timestamp.unwrap());
                println!();
            }
        })
        .await;

    Ok(())
}
