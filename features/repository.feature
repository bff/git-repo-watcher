Feature: Repository

  @broken
  Scenario: Executes script when git repo updated
    Given there is a remote git repo with a branch
    And a local commit for a script that curls a service
    When I push a commit to the branch of the repo
    Then the curl is executed
