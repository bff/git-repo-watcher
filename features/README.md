Playing with dockerized git server:

 1. Build the image:
    ```
    $ docker build features/ -t bff/git-ssh -f features/Dockerfile.git-ssh
    ```
 2. Create a local repo:
    ```
    $ mkdir example.git && cd example.git && git init
    ```
 3. Start the containerized git-ssh-server:
    ```
    $ docker run -it -p 2022:22 --rm bff/git-ssh
    ```
 4. Add a remote pointing to the container:
    ```
    $ git remote add origin ssh://git@localhost:2022/~/example.git
    ```
 5. Enjoy!
    ```
    $ GIT_SSH_COMMAND='ssh -i /path/to/features/id_rsa' git push origin master
    ```
