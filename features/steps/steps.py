@given(u'there is a git repo with a branch')
def step_impl(context):
    raise NotImplementedError(u'STEP: Given there is a git repo with a branch')


@given(u'a local commit for a script that curls a service')
def step_impl(context):
    raise NotImplementedError(u'STEP: Given a local commit for a script that curls a service')


@when(u'I push a commit to the branch of the repo')
def step_impl(context):
    raise NotImplementedError(u'STEP: When I push a commit to the branch of the repo')


@then(u'the curl is executed')
def step_impl(context):
    raise NotImplementedError(u'STEP: Then the curl is executed')
